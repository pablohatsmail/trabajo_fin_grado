enum AES_ERRORS{

  AES_ERROR_OK = 0,
  AES_ERROR_NULL_VALUE,
  AES_ERROR_OUT_RANGE,
  AES_ERROR_KEY_LENGHT

}typedef AES_ERRORS;

// MemoryFree library based on code posted here:
// http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1213583720/15
// Extended by Matthew Murdoch to include walking of the free list.

#ifndef  MEMORY_FREE_H
#define MEMORY_FREE_H

#ifdef __cplusplus
extern "C" {
#endif

int freeMemory();

#ifdef  __cplusplus
}
#endif

#endif

//,cpp
#if (ARDUINO >= 100)
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

extern unsigned int __heap_start;
extern void *__brkval;

/*
 * The free list structure as maintained by the
 * avr-libc memory allocation routines.
 */
struct __freelist
{
  size_t sz;
  struct __freelist *nx;
};

/* The head of the free list structure */
extern struct __freelist *__flp;

//#include "MemoryFree.h"

/* Calculates the size of the free list */
int freeListSize()
{
  struct __freelist* current;
  int total = 0;
  for (current = __flp; current; current = current->nx)
  {
    total += 2; /* Add two bytes for the memory block's header  */
    total += (int) current->sz;
  }

  return total;
}

int freeMemory()
{
  int free_memory;
  if ((int)__brkval == 0)
  {
    free_memory = ((int)&free_memory) - ((int)&__heap_start);
  }
  else
  {
    free_memory = ((int)&free_memory) - ((int)__brkval);
    free_memory += freeListSize();
  }
  return free_memory;
}
//END MEMORY LIBRARY

#define AES_KEY_BITS 128 //USED TO CHANGE AES 128 OR AES 256 ALGORITHM
#define AES_KEY_BYTES AES_KEY_BITS/8
#define SBOX_N_ELEM 16 //Matrix 16x16
#define ENCRYPT_MODE 0
#define DECRYPT_MODE 1

// xtime is a macro that finds the product of {02} and the argument to
// xtime modulo {1b} Galois Fields
#define xtime(x)   ((x<<1) ^ (((x>>7) & 1) * 0x1b))

/**
 * @brief Struct used to store rounds Keys in AES-128 decrypt
 */
struct KeyRound{

unsigned char Key[4][4];

}typedef KeyRound;

static unsigned char s_box[256] =
{
   0x63, 0x7C, 0x77, 0x7B, 0xF2, 0x6B, 0x6F, 0xC5, 0x30, 0x01, 0x67, 0x2B, 0xFE, 0xD7, 0xAB, 0x76,
   0xCA, 0x82, 0xC9, 0x7D, 0xFA, 0x59, 0x47, 0xF0, 0xAD, 0xD4, 0xA2, 0xAF, 0x9C, 0xA4, 0x72, 0xC0,
   0xB7, 0xFD, 0x93, 0x26, 0x36, 0x3F, 0xF7, 0xCC, 0x34, 0xA5, 0xE5, 0xF1, 0x71, 0xD8, 0x31, 0x15,
   0x04, 0xC7, 0x23, 0xC3, 0x18, 0x96, 0x05, 0x9A, 0x07, 0x12, 0x80, 0xE2, 0xEB, 0x27, 0xB2, 0x75,
   0x09, 0x83, 0x2C, 0x1A, 0x1B, 0x6E, 0x5A, 0xA0, 0x52, 0x3B, 0xD6, 0xB3, 0x29, 0xE3, 0x2F, 0x84,
   0x53, 0xD1, 0x00, 0xED, 0x20, 0xFC, 0xB1, 0x5B, 0x6A, 0xCB, 0xBE, 0x39, 0x4A, 0x4C, 0x58, 0xCF,
   0xD0, 0xEF, 0xAA, 0xFB, 0x43, 0x4D, 0x33, 0x85, 0x45, 0xF9, 0x02, 0x7F, 0x50, 0x3C, 0x9F, 0xA8,
   0x51, 0xA3, 0x40, 0x8F, 0x92, 0x9D, 0x38, 0xF5, 0xBC, 0xB6, 0xDA, 0x21, 0x10, 0xFF, 0xF3, 0xD2,
   0xCD, 0x0C, 0x13, 0xEC, 0x5F, 0x97, 0x44, 0x17, 0xC4, 0xA7, 0x7E, 0x3D, 0x64, 0x5D, 0x19, 0x73,
   0x60, 0x81, 0x4F, 0xDC, 0x22, 0x2A, 0x90, 0x88, 0x46, 0xEE, 0xB8, 0x14, 0xDE, 0x5E, 0x0B, 0xDB,
   0xE0, 0x32, 0x3A, 0x0A, 0x49, 0x06, 0x24, 0x5C, 0xC2, 0xD3, 0xAC, 0x62, 0x91, 0x95, 0xE4, 0x79,
   0xE7, 0xC8, 0x37, 0x6D, 0x8D, 0xD5, 0x4E, 0xA9, 0x6C, 0x56, 0xF4, 0xEA, 0x65, 0x7A, 0xAE, 0x08,
   0xBA, 0x78, 0x25, 0x2E, 0x1C, 0xA6, 0xB4, 0xC6, 0xE8, 0xDD, 0x74, 0x1F, 0x4B, 0xBD, 0x8B, 0x8A,
   0x70, 0x3E, 0xB5, 0x66, 0x48, 0x03, 0xF6, 0x0E, 0x61, 0x35, 0x57, 0xB9, 0x86, 0xC1, 0x1D, 0x9E,
   0xE1, 0xF8, 0x98, 0x11, 0x69, 0xD9, 0x8E, 0x94, 0x9B, 0x1E, 0x87, 0xE9, 0xCE, 0x55, 0x28, 0xDF,
   0x8C, 0xA1, 0x89, 0x0D, 0xBF, 0xE6, 0x42, 0x68, 0x41, 0x99, 0x2D, 0x0F, 0xB0, 0x54, 0xBB, 0x16
};

static char unsigned inv_s_box [256] =
{
  0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
  0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
  0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
  0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
  0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
  0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
  0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
  0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
  0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
  0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
  0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
  0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
  0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
  0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
  0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
  0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d
};

//Vector Rcon
static char unsigned rcon[15] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36, 0x6C, 0xD8 ,0xAB, 0x4D, 0x9A};


void MixColums(unsigned char * StateMatrix){

Serial.print("Start MixColums: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

unsigned char a[4];
unsigned char b[4];
unsigned char h; //high bit flag

  for(int i = 0; i < 4; i++){

    for (int c = 0; c < 4; c++) {
      a[c] = *(StateMatrix + 4*c + i);
      h = (unsigned char)((signed char)(*(StateMatrix + 4*c + i)) >> 7);
      b[c] = ((*(StateMatrix + 4*c + i)) << 1);
      b[c] ^= (0x1B & h); // Rijndael's Galois field
    }

  *(StateMatrix + 0 + i)   = b[0] ^ a[3] ^ a[2] ^ b[1] ^ a[1]; /* 2 * a0 + a3 + a2 + 3 * a1 */
  *(StateMatrix + 4*1 + i) = b[1] ^ a[0] ^ a[3] ^ b[2] ^ a[2]; /* 2 * a1 + a0 + a3 + 3 * a2 */
  *(StateMatrix + 4*2 + i) = b[2] ^ a[1] ^ a[0] ^ b[3] ^ a[3]; /* 2 * a2 + a1 + a0 + 3 * a3 */
  *(StateMatrix + 4*3 + i) = b[3] ^ a[2] ^ a[1] ^ b[0] ^ a[0]; /* 2 * a3 + a2 + a1 + 3 * a0 */

  }

Serial.print("End MixColums: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

void ShiftRows(unsigned char * StateMatrix){

Serial.print("Start ShiftRows: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

unsigned char aux_m [16] =

  {
    *(StateMatrix),           *(StateMatrix + 1),       *(StateMatrix + 2),       *(StateMatrix + 3),
    *(StateMatrix + 1*4 + 1), *(StateMatrix + 1*4 +2),  *(StateMatrix + 1*4 + 3), *(StateMatrix + 1*4),
    *(StateMatrix + 2*4 + 2), *(StateMatrix + 2*4 + 3), *(StateMatrix + 2*4),     *(StateMatrix + 2*4 + 1),
    *(StateMatrix + 3*4 + 3), *(StateMatrix + 3*4),     *(StateMatrix + 3*4 + 1), *(StateMatrix + 3*4 + 2),
  };

for(int i = 0; i < (AES_KEY_BITS/8); i++){

  *(StateMatrix + i) = *(aux_m + i);

}

Serial.print("End ShiftRows: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

void SubBytes(unsigned char * StateMatrix){

Serial.print("Start SubBytes: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

int low_nibble = 0;
int hihg_nibble = 0;

for(int i = 0; i < (AES_KEY_BITS/32); i++){

  for(int j = 0; j < (AES_KEY_BITS/32); j++ ){

    low_nibble = (*(StateMatrix + i*(AES_KEY_BITS/32) + j)) & 0x0f;
    hihg_nibble = ((*(StateMatrix + i*(AES_KEY_BITS/32) + j)) >> 4) & 0x0f;

    *(StateMatrix + i*(AES_KEY_BITS/32) + j)  = s_box[(hihg_nibble * SBOX_N_ELEM) + low_nibble];

  }
}

Serial.print("End SubBytes: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

//Make XOR operation between (KeyMatrix XOR StateMatrix) obtain result on ResultXORMatrix
void AddRoundKey(unsigned char * KeyMatrix, unsigned char * StateMatrix){

Serial.print("Start AddRoundKey: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

for(int i = 0; i < (AES_KEY_BITS/32); i++){

  for(int j = 0; j < (AES_KEY_BITS/32); j++ ){

    *(StateMatrix + i*(AES_KEY_BITS/32) + j)  = (*(KeyMatrix + i*(AES_KEY_BITS/32) + j)) ^ (*(StateMatrix + i*(AES_KEY_BITS/32) + j));

  }
}

Serial.print("End AddRoundKey: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

void InvMixColums(unsigned char * StateMatrix){

Serial.print("Start InvMixColums: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

unsigned char a[4] = {0};//Colum x1
unsigned char b[4] = {0};//Colum x2
unsigned char c[4] = {0};//Colum x4
unsigned char d[4] = {0};//Colum x8

  for(int i = 0; i < 4; i++){

    for (int j = 0; j < 4; j++) {

      a[j] = *(StateMatrix + 4*j + i);

      b[j] = xtime(a[j]);

      c[j] = xtime(b[j]);

      d[j] = xtime(c[j]);

    }

  *(StateMatrix + 0 + i)   = (d[0] ^ c[0] ^ b[0]) ^ (d[1] ^ b[1] ^ a[1]) ^ (d[2] ^ c[2] ^ a[2]) ^ (d[3] ^ a[3]);
  *(StateMatrix + 4*1 + i) = (d[0] ^ a[0]) ^ (d[1] ^ c[1] ^ b[1]) ^ (d[2] ^ b[2] ^ a[2]) ^ (d[3] ^ c[3] ^ a[3]);
  *(StateMatrix + 4*2 + i) = (d[0] ^ c[0] ^ a[0]) ^ (d[1] ^ a[1]) ^ (d[2] ^ c[2] ^ b[2]) ^ (d[3] ^ b[3] ^ a[3]);
  *(StateMatrix + 4*3 + i) = (d[0] ^ b[0] ^ a[0]) ^ (d[1] ^ c[1] ^ a[1]) ^ (d[2] ^ a[2]) ^ (d[3] ^ c[3] ^ b[3]);

  }

Serial.print("End InvMixColums: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

void InvShiftRows(unsigned char * StateMatrix){
Serial.print("Start InvShiftRows: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

  unsigned char aux_m [16] =

    {
      *(StateMatrix),           *(StateMatrix + 1),       *(StateMatrix + 2),       *(StateMatrix + 3),
      *(StateMatrix + 1*4 + 3), *(StateMatrix + 1*4),   *(StateMatrix + 1*4 + 1), *(StateMatrix + 1*4 +2),
      *(StateMatrix + 2*4 + 2), *(StateMatrix + 2*4 + 3), *(StateMatrix + 2*4),     *(StateMatrix + 2*4 + 1),
      *(StateMatrix + 3*4 + 1), *(StateMatrix + 3*4 + 2), *(StateMatrix + 3*4 + 3), *(StateMatrix + 3*4),
    };

  for(int i = 0; i < (AES_KEY_BITS/8); i++){

    *(StateMatrix + i) = *(aux_m + i);

  }

Serial.print("End InvShiftRows: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

void InvSubBytes(unsigned char * StateMatrix){

Serial.print("Start InvSubBytes: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());


int low_nibble = 0;
int hihg_nibble = 0;

  for(int i = 0; i < (AES_KEY_BITS/32); i++){

    for(int j = 0; j < (AES_KEY_BITS/32); j++ ){

      low_nibble = (*(StateMatrix + i*(AES_KEY_BITS/32) + j)) & 0x0f;
      hihg_nibble = ((*(StateMatrix + i*(AES_KEY_BITS/32) + j)) >> 4) & 0x0f;

      *(StateMatrix + i*(AES_KEY_BITS/32) + j)  = inv_s_box[(hihg_nibble * SBOX_N_ELEM) + low_nibble];

    }
  }

Serial.print("End InvSubBytes: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

void KeyExpansion(unsigned char * KeyMatrix, unsigned char * NextKeyMatrix, int round){

Serial.print("Start KeyExpansion: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
    
unsigned char m_aux[16];

  //RotWord
  m_aux[0] = *(KeyMatrix + 4 + 3);
  m_aux[4] = *(KeyMatrix + 4*2 + 3);
  m_aux[4*2] = *(KeyMatrix + 4*3 + 3);
  m_aux[4*3] = *(KeyMatrix + 3);

  //Subytes
  SubBytes((unsigned char *)m_aux);

  //XOR i-3 XOR Vector Rcon
  m_aux[0] ^= *(KeyMatrix) ^ rcon[round];
  m_aux[4] ^= *(KeyMatrix + 4);
  m_aux[4*2] ^= *(KeyMatrix + 4*2);
  m_aux[4*3] ^= *(KeyMatrix + 4*3);

  //XOR i-3 for n-1
  for(int j = 1; j<4; j++){

    m_aux[j] = *(KeyMatrix + (j)) ^ m_aux[j - 1];
    m_aux[4 + j] = *(KeyMatrix + 4 + (j)) ^ m_aux[4 + j - 1];
    m_aux[4*2 + j] = *(KeyMatrix + 4*2 + (j)) ^ m_aux[4*2 + j - 1];
    m_aux[4*3 + j] = *(KeyMatrix + 4*3 + (j)) ^ m_aux[4*3 + j - 1];
  }

  //Copy m_aux to m_key
  for(int i = 0; i < (AES_KEY_BITS/8); i++){
    *(NextKeyMatrix + i) = *(m_aux + i);
  }


Serial.print("End KeyExpansion: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

//Do AES-128 Encrypt process
void AES128Encrypt(unsigned char * PlainText, unsigned char * Key, unsigned char * Cryptogram){

Serial.print("Start AES128Encrypt: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

unsigned char StateMatrix [4][4] =
{
  {PlainText[0], PlainText[4], PlainText[8], PlainText[12]},
  {PlainText[1], PlainText[5], PlainText[9], PlainText[13]},
  {PlainText[2], PlainText[6], PlainText[10], PlainText[14]},
  {PlainText[3], PlainText[7], PlainText[11], PlainText[15]},
};

//For 128 key matrix bits AES-128
unsigned char KeyMatrix [4][128/32] =
{
  {Key[0], Key[4], Key[8], Key[12]},
  {Key[1], Key[5], Key[9], Key[13]},
  {Key[2], Key[6], Key[10], Key[14]},
  {Key[3], Key[7], Key[11], Key[15]},
};

//For 256 key matrix bits AES-256. Only for AES-256 bits usage.
#if AES_KEY_BITS == 256
unsigned char KeyMatrix [4][128/32] =
{
    {Key[16], Key[20], Key[24], Key[28]},
    {Key[17], Key[21], Key[25], Key[29]},
    {Key[18], Key[22], Key[26], Key[30]},
    {Key[19], Key[23], Key[27], Key[31]},
  };
#endif

  //Start AES-128 process
  //First Round (Round 0) PlainText Key (Ko) is used
  AddRoundKey((unsigned char *)KeyMatrix, (unsigned char *)StateMatrix);

  for(int i = 0; i<9; i++){ //Round 1 to 9 (used K1 to k9 Keys)

    KeyExpansion((unsigned char *)KeyMatrix, (unsigned char *)KeyMatrix, i); //Key expansion

    SubBytes((unsigned char *)StateMatrix);

    ShiftRows((unsigned char *)StateMatrix);

    MixColums((unsigned char *)StateMatrix);

    AddRoundKey((unsigned char *)KeyMatrix, (unsigned char *)StateMatrix);

  }

  //Final round
  KeyExpansion((unsigned char *)KeyMatrix, (unsigned char *)KeyMatrix, 9); //Expansion Key Round 10

  SubBytes((unsigned char *)StateMatrix);

  ShiftRows((unsigned char *)StateMatrix);

  AddRoundKey((unsigned char *)KeyMatrix, (unsigned char *)StateMatrix);
  //End AES-128 process

  //Copy to output string in readable order
  for(int i = 0; i<4; i++){
    for(int j = 0; j<4; j++){
      *(Cryptogram + i*4 + j) = *((unsigned char *)StateMatrix + j*4 + i);
    }
  }
  
Serial.print("End AES-128 Encrypt: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

void AES128Decrypt(unsigned char * Cryptogram, unsigned char * Key, unsigned char * PlainText){

Serial.print("Start AES-128 Decrypt: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

unsigned char StateMatrix [4][4] =
{
  {Cryptogram[0], Cryptogram[4], Cryptogram[8],  Cryptogram[12]},
  {Cryptogram[1], Cryptogram[5], Cryptogram[9],  Cryptogram[13]},
  {Cryptogram[2], Cryptogram[6], Cryptogram[10], Cryptogram[14]},
  {Cryptogram[3], Cryptogram[7], Cryptogram[11], Cryptogram[15]},
};

//For 128 key matrix bits AES-128
unsigned char KeyMatrix [4][128/32] =
{
  {Key[0], Key[4], Key[8], Key[12]},
  {Key[1], Key[5], Key[9], Key[13]},
  {Key[2], Key[6], Key[10], Key[14]},
  {Key[3], Key[7], Key[11], Key[15]},
};

struct KeyRound s_KeyRound[10]; //Store K1 to K10 expanded keys

  /*Start AES-128 decrypt process*/

  //Precalculation of Kn keys expanded for next usage on decrypt
  KeyExpansion((unsigned char *)KeyMatrix,(unsigned char *)s_KeyRound[0].Key, 0);

  for(int i = 1; i<10; i++)
  KeyExpansion((unsigned char *)s_KeyRound[i-1].Key,(unsigned char *)s_KeyRound[i].Key, i);

  //AES-128 bits FIRST ROUND decrypt. Key n10 ([9]) will be used (inverse order that encrypt).
  AddRoundKey((unsigned char *)s_KeyRound[9].Key, (unsigned char *)StateMatrix);

  InvShiftRows((unsigned char *)StateMatrix);

  InvSubBytes((unsigned char *)StateMatrix);

  //Round 1 to 9 (used K1 to k9 Keys)
  for(int i = 8; i>=0; i--){

    AddRoundKey((unsigned char *)s_KeyRound[i].Key, (unsigned char *)StateMatrix);

    InvMixColums((unsigned char *)StateMatrix);

    InvShiftRows((unsigned char *)StateMatrix);

    InvSubBytes((unsigned char *)StateMatrix);

  }

  //Final round
  AddRoundKey((unsigned char *)KeyMatrix, (unsigned char *)StateMatrix);
  //End AES-128 process

  //Copy to output string in readable order
  for(int i = 0; i<4; i++){
    for(int j = 0; j<4; j++){
      *(PlainText + i*4 + j) = *((unsigned char *)StateMatrix + j*4 + i);
    }
  }

Serial.print("End AES-128 Decrypt: ");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
};

AES_ERRORS AES128(unsigned char * InputString, unsigned char * Key, int mode, unsigned char * OutputString){

Serial.print("Start AES-128 Algorithm");
Serial.print("freeMemory()=");
Serial.println(freeMemory());

AES_ERRORS e_ret = AES_ERROR_OK;


  if(mode == ENCRYPT_MODE){
    AES128Encrypt(InputString, Key, OutputString);
  }
  if(mode == DECRYPT_MODE){
    AES128Decrypt(InputString, Key, OutputString);
  }

Serial.print("End AES-128 Algorithm");
Serial.print("freeMemory()=");
Serial.println(freeMemory());
return e_ret;
};

////////////////////-////////////TESTING MAIN FUCNTION/////////////////////////////////////////
void setup() {
  
  Serial.begin(9600);
  unsigned char CipherText[AES_KEY_BYTES] = {0};
  char sTime [20];
  unsigned long TimeStart = 0;
  unsigned long TimeEnd = 0;
  //Start AES-128 Algorithm time

  TimeStart = millis();
  
  //////////////////////////////AES-128 Algorithm/////////////////////////////////////////////
  AES128((unsigned char *)"\x32\x43\xF6\xA8\x88\x5A\x30\x8D\x31\x31\x98\xA2\xE0\x37\x07\x34",
      (unsigned char *)"\x2B\x7E\x15\x16\x28\xAE\xD2\xA6\xAB\xF7\x15\x88\x09\xCF\x4F\x3C",
      ENCRYPT_MODE, CipherText);
      
  TimeEnd = millis();

   
  Serial.println("Start/End AES-128 Algorithm time");
    sprintf( sTime, "%04X\t%ld", TimeStart, TimeStart);
  Serial.println(sTime);
  sprintf( sTime, "%04X\t%ld", TimeEnd, TimeEnd);
  Serial.println(sTime);

  //Print Results
  Serial.println("Result Encrypt/Decrypt AES-128");

  //Print result on ASCII format
  Serial.println("ASCII Result:");
  Serial.write(CipherText,16);

  //Print Result on decimal format
  Serial.println("");
  Serial.println("DEC Result: ");
  Serial.println((long unsigned int)CipherText[0]);
  Serial.println((long unsigned int)CipherText[1]);
  Serial.println((long unsigned int)CipherText[2]);
  Serial.println((long unsigned int)CipherText[3]);
  Serial.println((long unsigned int)CipherText[4]);
  Serial.println((long unsigned int)CipherText[5]);
  Serial.println((long unsigned int)CipherText[6]);
  Serial.println((long unsigned int)CipherText[7]);
}

void loop() {
  // put your main code here, to run repeatedly:

}
